@extends('layouts.master')

@section('title', 'Students')

@section('page_header', "Student's List")

@section('breadcrumb', "Students")


@section('content')
    <a href="{{ route('studentRegistrationForm') }}" class="btn btn-warning">Register</a>
    <hr>
    <h3>All Students</h3>


@endsection

@push('css')

<style>
    .container{
        background: skyblue;
    }
</style>

@endpush